mod args;
mod log_path;
mod parser_async;

use std::sync::Arc;


use tokio::sync::mpsc;


#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    
    let paths = args::input();
    let logs_pool = log_path::find_logs(paths);

    let mut subject = parser_async::LogSubject::new();
    let running_observer = parser_async::RunningInfo {};
    subject.attach(Box::new(running_observer));
    let arc_subject = Arc::new(subject);

   
    let mut recs = vec![];

    for log in logs_pool {
        let (tx,  rx) = mpsc::channel(100);
        recs.push((log.clone(),rx));
        let subject = arc_subject.clone();
        tokio::spawn( async move {
            subject.process_log(&log, &tx).await;
        });
    }

    for (name, mut recv)  in recs {
        while let Some(result) = recv.recv().await {
            println!("main recv: [{}]{}", name, result);
        }
    }

    //let results = parser_async::process_logs(logs_pool).await;
    //for result in results {
    //    println!("{}", result);
    //}

    Ok(())
    
}
